<?php

if (!isset($_SESSION['sc_caminho'])){
	$_SESSION['sc_caminho']=getcwd();
	$tmp_path=substr($_SERVER['PHP_SELF'],1);
	$_SESSION['sc_address']='/'.substr($tmp_path,0,strrpos($tmp_path,'/'));
}
$_SESSION['sc_main_path'] = $_SESSION['sc_address'].'/../';
 
include $_SESSION['sc_caminho']."/../config.php";
include $_SESSION['sc_caminho']."/../versao.php";

global $con;

$GLOBALS['Plocation']='';
$GLOBALS['db']=$database_type;
if (strtolower($db)=='mssql') {$c_db='m';}
if (strtolower($db)=='mysql') {$c_db='y';}
if (strtolower($db)=='sybase') {$c_db='s';}
if (strtolower($db)=='oracle') {$c_db='o';}
$con=tw_connect( $database_server
			   , $database_username
			   , $database_password
			   , $database_name
			   , $database_port
);
$_SESSION['db'] = $database_name;
tw_select_db($database_name,$con);

$_SESSION['con'] = $con;

if (!isset($main))
{
	if (!isset($_SESSION['Pusuario'])){
		if (isset($_REQUEST['senha'])){
			exit("Inicie a sessao efetuando logon na tela principal");
		}
	}
}
