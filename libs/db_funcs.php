<?php
//*****************************************************************************
#Funções para DB (Henrique Oliveira)
#Funçõo de Pesquisa e execuçõo em Banco(Query)
date_default_timezone_set("America/Fortaleza");
function tw_upper($sql){
	$i=0;
	$s='';
	$x=0;
	$t=strlen($sql);
	while($i<$t){
		if(substr($sql,$i,1)=="'"){
			if($x==0)
				$x=1;
			else
				$x=0;
		}
		if($x==0){
			$s.=strtoupper(substr($sql,$i,1));
		}else
			$s.=substr($sql,$i,1);
		$i+=1;
	}
	return $s;
}

//Retorna o Ultimo ID adicionado pelo Autoincremento
function tw_last_id($pc_tabela, $pc_campo){
	if ($GLOBALS['db']=='mysql'){
		$sql="select $pc_campo from $pc_tabela 
			   where $pc_campo = LAST_INSERT_ID()";
	}else if ($GLOBALS['db']=='mysqli'){
		$sql="select $pc_campo from $pc_tabela 
			   where $pc_campo = LAST_INSERT_ID()";
	}else if ($GLOBALS['db']=='mssql' || $GLOBALS['db']=='sqlserver'){
		$sql="select $pc_campo from $pc_tabela 
			   where $pc_campo = @@identity";
	}else if ($GLOBALS['db']=='oracle'){
		$sql="select seq_".$pc_tabela.".CURRVAL from dual";
	}
	// echo $sql;
	$qry=tw_query($sql,$_SESSION['con']);
	$row=tw_fetch_row($qry);
	return $row[0];
}

function tw_query($sql,$conexao,$faz = true){
	//echo "<pre>\n\n $sql \n\n";
	if(!isset($_SESSION['MOSTRA'])){
		$_SESSION['MOSTRA']=0;
	}
	if ($_SESSION['MOSTRA'] == 1){
		echo '<br><div style="color:red;">'.$sql.'</div><br>';
	}
	if ($GLOBALS['db']=='dblib'){
		$qry = $conexao->prepare($sql);
		$qry->execute();
	}
	if ($GLOBALS['db']=='sybase'){
		$sql = preg_replace('/\ [Tt][Rr][Ii][Mm]/', 'RTRIM', $sql);
		$qry 	= sybase_query(tw_upper($sql),$conexao);
		$errno	= sybase_error();
		$erro	= sybase_errno();
	}else
	if ($GLOBALS['db']=='mysqli'){
		$qry = mysqli_query($sql,$conexao);
		$erro	= mysqli_error();
		$errno	= mysqli_errno();
	}else
	if ($GLOBALS['db']=='mysql'){
		$qry = mysql_query($sql,$conexao);
		$erro	= mysql_error();
		$errno	= mysql_errno();
	}else
	if ($GLOBALS['db']=='mssql'){
		$sql = preg_replace('/[Tt][Rr][Ii][Mm]/', 'RTRIM', $sql);
                //$sql = preg_replace('/[Tt][Rr][Ii]/', 'RTRIM', $sql);
		$qry = mssql_query($sql,$conexao);
		$errno	= '';
		$erro	= '';
	}else
	if ($GLOBALS['db']=='sqlserver'){
		$sql = preg_replace('/[Tt][Rr][Ii][Mm]/', 'RTRIM', $sql);
                //$sql = preg_replace('/[Tt][Rr][Ii]/', 'RTRIM', $sql);
		$qry = sqlsrv_query($conexao,$sql, array(), array( "Scrollable" => 'static' ));
		$errno	= '';
		$erro	= '';
	}else
	if ($GLOBALS['db']=='odbc'){
		$qry = odbc_exec($conexao,$sql);
	}else if ($GLOBALS['db']=='oracle'){
		$res=ociparse($conexao,$sql);
		$ret=ociexecute($res,OCI_DEFAULT);
		if  ( (strtoupper(substr(trim($sql),0,6))!='SELECT') && (strtoupper(substr(trim($sql),0,6))!='COMMIT')) {
			if (!isset($_SESSION['sc_BT'])){
				tw_commit_transaction($conexao);
			}
		}else{
			if (strtoupper(substr(trim($sql),0,6))=='SELECT'){
				$GLOBALS['sqlora_dois']=$sql;
			}
		}
		if (!$ret){
			$qry=false;
		}else{
			$qry=$res;
		}
	}else if($GLOBALS['db']=='db2'){
		$qry = db2_exec($con, $sql, array('cursor' => DB2_SCROLLABLE));
	}
	if ( (!$qry) && ($faz) ){
		//if (strtoupper(substr(trim($sql),0,6))<>'SELECT'){
                     
                        // @atv_erro  - false ou true
                        // exibe ou n䯠exibe erros

                        $_SESSION['ativa_dbfuncs'];
                        $ativa_dbfuncs = $_SESSION['ativa_dbfuncs'];
                        
//                        function error($numero,$texto){ 
//                                $ddf = fopen('error.log','a'); 
//                                fwrite($ddf,"[".date("r")."] Error $numero: $texto\r\n"); 
//                                fclose($ddf); 
//                            } 
//                            error('Erro de query',$sql);
                        if(isset($ativa_dbfuncs) && $ativa_dbfuncs!= "" && $ativa_dbfuncs == "true"){
                            echo '<br><div style="color:red;">Erro: <pre>'.$sql.'</div><br>';exit;
                            
                       
 
                        }
                        
/*d*/			//echo '<br><div style="color:red;">Erro: <pre>'.$sql.'</div><br>';exit;
			//tw_errosql($sql,$errno,$erro);
			//die;exit;
		//}
	}
	return $qry;
}

function tw_fetch_row($res)
{
	if ($GLOBALS['db']=='dblib'){
		return $res->fetch(PDO::FETCH_NUM);
}
	if ($GLOBALS['db']=='sybase'){
		return sybase_fetch_row($res);
	}else
	if ($GLOBALS['db']=='mysql'){
		return mysql_fetch_row($res);
	}else
	if ($GLOBALS['db']=='mysqli'){
		return mysqli_fetch_row($res);
	}else
	if ($GLOBALS['db']=='mssql'){
		return mssql_fetch_row($res);
	}else
	if ($GLOBALS['db']=='sqlserver'){
		return sqlsrv_fetch_array($res,SQLSRV_FETCH_NUMERIC);
	}else
	if ($GLOBALS['db']=='oracle'){
		ocifetchinto($res,$ret,OCI_RETURN_NULLS + OCI_NUM);
		return $ret;
	}else
	if ($GLOBALS['db']=='db2'){
		return db2_fetch_row($res);
	}else
	if ($GLOBALS['db']=='odbc'){
		$ret = odbc_fetch_row($res);
		if(!$ret) return false;
		$nfields = odbc_num_fields($res);
		for($i=0;$i<$nfields;$i++){
			$row[$i] = odbc_result($res,$i+1);
		}
		return $row;
	}
}

function tw_fetch_array($res)
{
	if ($GLOBALS['db']=='dblib'){
                return $res->fetch(PDO::FETCH_ASSOC);
        }else
	if ($GLOBALS['db']=='sybase'){
		return sybase_fetch_array ($res);
	}else
	if ($GLOBALS['db']=='mysql'){
		return mysql_fetch_array($res);
	}else
	if ($GLOBALS['db']=='mysqli'){
		return mysqli_fetch_array($res);
	}else
	if ($GLOBALS['db']=='mssql'){
		return mssql_fetch_array($res);
	}else
	if ($GLOBALS['db']=='sqlserver'){
		return sqlsrv_fetch_array($res,SQLSRV_FETCH_BOTH);
	}else
	if ($GLOBALS['db']=='oracle'){
		ocifetchinto($res,$ret,OCI_RETURN_NULLS + OCI_ASSOC);
		return $ret;
	}else
	if ($GLOBALS['db']=='db2'){
		return db2_fetch_array($res);
	}else
	if ($GLOBALS['db']=='odbc'){
		return odbc_fetch_array($res);
	}
}

function tw_fetch_assoc($res)
{
	if ($GLOBALS['db']=='dblib'){
                return $res->fetch(PDO::FETCH_ASSOC);
        }else
	if ($GLOBALS['db']=='sybase'){
		return sybase_fetch_assoc ($res);
	}else
	if ($GLOBALS['db']=='mysql'){
		return mysql_fetch_assoc($res);
	}else
	if ($GLOBALS['db']=='mysqli'){
		return mysqli_fetch_assoc($res);
	}else
	if ($GLOBALS['db']=='mssql'){
		return mssql_fetch_assoc($res);
	}else
	if ($GLOBALS['db']=='sqlserver'){
		return sqlsrv_fetch_array($res,SQLSRV_FETCH_ASSOC);
	}else
	if ($GLOBALS['db']=='oracle'){
		ocifetchinto($res,$ret,OCI_RETURN_NULLS + OCI_ASSOC);
		return $ret;
	}else
	if ($GLOBALS['db']=='db2'){
		return db2_fetch_assoc($res);
	}else
	if ($GLOBALS['db']=='odbc'){
		return odbc_fetch_assoc($res);
	}
}

function tw_num_rows($res){
	if ($GLOBALS['db']=='dblib'){
                return $res->rowCount();
        }else if ($GLOBALS['db']=='sybase'){
		return sybase_num_rows($res);
	}else if ($GLOBALS['db']=='mysql'){
		return mysql_num_rows($res);
	}else if ($GLOBALS['db']=='mysqli'){
		return mysqli_num_rows($res);
	}else if ($GLOBALS['db']=='mssql'){
		return mssql_num_rows($res);
	}else if ($GLOBALS['db']=='sqlserver'){
		return sqlsrv_num_rows($res);
	}else if ($GLOBALS['db']=='oracle'){
		$res=ociparse($_SESSION['con'],$GLOBALS['sqlora_dois']);
		ociexecute($res,OCI_DEFAULT);
		ocifetchstatement($res, $results,0,-1,OCI_FETCHSTATEMENT_BY_ROW);
		return count($results);
	}else
	if($GLOBALS['db']=='db2'){
		return db2_num_rows($res);
	}else
	if ($GLOBALS['db']=='odbc'){
		return odbc_num_rows($res);
	}
}
function tw_data_seek($res,$i){
	if ($GLOBALS['db']=='sybase'){
		sybase_data_seek ($res, $i);
	}else if($GLOBALS['db']=='mysql'){
		mysql_data_seek($res, $i);
	}else if($GLOBALS['db']=='mysqli'){
		mysqli_data_seek($res, $i);
	}else if ($GLOBALS['db']=='mssql'){
		mssql_data_seek($res, $i);
	}else if ($GLOBALS['db']=='sqlserver'){
		sqlsrv_fetch($res,SQLSRV_SCROLL_RELATIVE,$i);
	}else if ($GLOBALS['db']=='oracle'){
		for ($v_cont=0;$v_cont<$i;$v_cont++) {$row=tw_fetch_row($res);}
	}else
	if($GLOBALS['db']=='db2'){
		for ($v_cont=0;$v_cont<$i;$v_cont++) {$row=db2_fetch_row($res);}
	}else
	if ($GLOBALS['db']=='odbc'){
		for ($v_cont=0;$v_cont<$i;$v_cont++) {$row=odbc_fetch_row($res);}
	}
}
function tw_affected_rows($res){
	if ($GLOBALS['db']=='sybase'){
		$ret=sybase_affected_rows ($res);
	}else if ($GLOBALS['db']=='mysql'){
		$ret=mysql_affected_rows($res);
	}else if ($GLOBALS['db']=='mysqli'){
		$ret=mysqli_affected_rows($res);
	}else if ($GLOBALS['db']=='mssql'){
		if (function_exists('mssql_rows_affected')) {
			return mssql_rows_affected($res);
		} else {
			$result = mssql_query("select @@rowcount as rows", $res);
			$rows = mssql_fetch_assoc($result);
			return $rows['rows'];
		}
	}else if ($GLOBALS['db']=='sqlserver'){
		if (function_exists('sqlsrv_rows_affected')) {
			return sqlsrv_rows_affected($res);
		} else {
			$result = sqlsrv_query($res,"select @@rowcount as rows");
			$rows = sqlsrv_fetch_array($result,SQLSRV_FETCH_ASSOC);
			return $rows['rows'];
		}
	}else if ($GLOBALS['db']=='oracle'){
		if (is_resource($res)){
			$ret=oci_num_rows($res);
		}
	}else
	if($GLOBALS['db']=='db2'){
		$ret=db2_num_rows($res);
	}else
	if ($GLOBALS['db']=='odbc'){
		$ret = odbc_num_rows($res);
	}
	return $ret;
}

function tw_connect($host,$user,$password,$db_name,$pn_port = 0){
	if ($GLOBALS['db']=='dblib'){
		$conexao = new PDO( "dblib:host=autologessencial.caakfysii0mk.us-east-1.rds.amazonaws.com:4331;dbname=Autolog_Essencial;","admin","Twco.2020*");

	}
	if ($GLOBALS['db']=='mysql'){
		$conexao=mysql_connect($host,$user,$password)
		or exit("Erro ao tentar se conectar ao Banco de Dados : ".mysql_error());// host, usuario e senha do mysql
		//tw_query("SET character_set_results=cp1250, character_set_connection=cp1250, character_set_client=cp1250",$conexao);
	}else if ($GLOBALS['db']=='mysqli'){
		$conexao=mysqli_connect($host,$user,$password)
		or exit("Erro ao tentar se conectar ao Banco de Dados : ".mysql_error());// host, usuario e senha do mysql
		//tw_query("SET character_set_results=cp1250, character_set_connection=cp1250, character_set_client=cp1250",$conexao);
	}else if ($GLOBALS['db']=='oracle'){
		if ($pn_port==0){$pn_port=1521;}
		if (substr($db_name,0,1)!='('){
			$db_tns ="(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP) (HOST = ".$host.") (PORT = ".$pn_port.") ) (CONNECT_DATA = (SID = ".$db_name.")) )";
		}else{
			$db_tns = $db_name;
		}
		$conexao=ociplogon($user,$password,$db_tns);
		//tw_query('ALTER SYSTEM SET PROCESSES=150 SCOPE=SPFILE',$conexao); // TEMPORARIO, SOLUCAO APENAS PARA ORACLE DO MARCOS< COMENTAR MAIS ADIANTE
        //O comando abaixo muda para Ponto os campos Num곩cos.
        tw_query("ALTER SESSION SET NLS_NUMERIC_CHARACTERS = '.,'",$conexao);
	}else if ($GLOBALS['db']=='mssql'){
		$conexao=mssql_connect($host,$user,$password,1);
	}else if ($GLOBALS['db']=='sqlserver'){
		$connectionInfo = array( "Database"=>$db_name, "UID"=>$user, "PWD"=>$password);
		$conexao=sqlsrv_connect($host,$connectionInfo);
		if($conexao === false){
			die (print_r(sqlsrv_errors(), true));
		}
	}else if ($GLOBALS['db']=='sybase'){
		$conexao=sybase_connect($host,$user,$password);
	}else
	if($GLOBALS['db']=='db2'){
		if((int)$pn_port==0){
			$pn_port=50000;
		}
		$dsn = "DATABASE=$dn_name;HOSTNAME=$host;PORT=$pn_port;
		  PROTOCOL=TCPIP;UID=$user;PWD=$password;";

		$options = array('autocommit' => DB2_AUTOCOMMIT_ON);
		$conexao = db2_connect($dns,"","",$options);
	}else
	if ($GLOBALS['db']=='odbc'){
		$conexao = odbc_connect($host,$user,$password);
	}
	return $conexao;
}


function tw_select_db($banco,$conexao){
	if ($GLOBALS['db']=='mysql'){
		$conexao_bd = mysql_select_db($banco,$conexao)
		or exit("Erro ao Usar BD : ".mysql_error());//� o nome da base de Dados
	}else if ($GLOBALS['db']=='mysqli'){
		$conexao_bd = mysqli_select_db($banco,$conexao)
		or exit("Erro ao Usar BD : ".mysql_error());//� o nome da base de Dados
	}else if ($GLOBALS['db']=='oracle'){
		$conexao_bd = true;
	}else if ($GLOBALS['db']=='mssql'){
		$conexao_bd = mssql_select_db($banco,$conexao);
		//or exit("Erro ao Usar BD : ".mysql_error());//� o nome da base de Dados
	}else if ($GLOBALS['db']=='sqlserver'){
		$conexao_bd = true;
		//or exit("Erro ao Usar BD : ".mysql_error());//� o nome da base de Dados
	}else if ($GLOBALS['db']=='sybase'){
		$conexao_bd = sybase_select_db($banco,$conexao)
		or exit("Erro ao Usar BD : ".mysql_error());//� o nome da base de Dados
	}else
	if($GLOBALS['db']=='db2'){
		
	}else
	if ($GLOBALS['db']=='odbc'){
		
	}
	return $conexao_bd;
}

function tw_toupper(){
	if ($GLOBALS['db']=='mysql'){
		$text='upper';
	}else if ($GLOBALS['db']=='oracle'){
		$text='upper';
	}else if ($GLOBALS['db']=='mssql'){
		$text='upper';
	}else if ($GLOBALS['db']=='sybase'){
		$text='upper';
	}else if ($GLOBALS['db']=='db2'){
		$text='upper';
	}else
	if ($GLOBALS['db']=='odbc'){
		$text="upper";
	}
	return $text;
}

function tw_substr(){
	if ($GLOBALS['db']=='mysql'){
		$text='substr';
	}else if ($GLOBALS['db']=='oracle'){
		$text='substr';
	}else if ($GLOBALS['db']=='mssql'){
		$text='substring';
	}else if ($GLOBALS['db']=='sybase'){
		$text='substr';
	}else if ($GLOBALS['db']=='db2'){
		$text='substr';
	}
	return $text;
}
function tw_cast($campo,$tipo,$tamanho=100){
	if ($GLOBALS['db']=='mysql'){
		$text="cast($campo AS $tipo)";
	}else if ($GLOBALS['db']=='oracle'){
		$text="cast($campo AS $tipo($tamanho))";
	}else if ($GLOBALS['db']=='mssql'){
		$text="cast($campo AS $tipo)";
	}
	return $text;
}

function tw_length(){
	if ($GLOBALS['db']=='mysql'){
		$text='length';
	}else if ($GLOBALS['db']=='oracle'){
		$text='length';
	}else if ($GLOBALS['db']=='mssql'){
		//$text='datalength';
        $text='len';
	}else if ($GLOBALS['db']=='sybase'){
		$text='datalength';
	}else if ($GLOBALS['db']=='db2'){
		$text='length';
	}
	return $text;
}

function tw_begin_transaction(){
	if ($GLOBALS['db']=='mssql'){
		tw_query('begin transaction',$_SESSION['con']);
	}else if ($GLOBALS['db']=='sqlserver'){
		sqlsrv_begin_transaction($_SESSION['con']);
	}
	$_SESSION['sc_BT']='1';
}
function tw_commit_transaction($con10 = 0){
	if($con10==0){
		$con10 = $_SESSION['con'];
	}
	if ($GLOBALS['db']=='oracle'){
		oci_commit($con10);
	}else if ($GLOBALS['db']=='sqlserver'){
		sqlsrv_commit($con10);
	}else{
		tw_query('commit',$con10);
	}
	unset($_SESSION['sc_BT']);
}
function tw_rollback_transaction($con10 = 0){
	if($con10==0){
		$con10 = $_SESSION['con'];
	}
	if ($GLOBALS['db']=='oracle'){
		oci_rollback($con10);
	}else if ($GLOBALS['db']=='sqlserver'){
		sqlsrv_rollback($con10);
	}else{
		tw_query('rollback',$con10);
	}
	unset($_SESSION['sc_BT']);
}

function odbc_fetch_assoc($rs)
{
    if (odbc_fetch_row($rs))
    {

	$line = array("odbc_affected_rows" => odbc_num_rows($rs));
	
	for ($f = 1; $f <= odbc_num_fields($rs); $f++)
	{

	    $fn = odbc_field_name($rs, $f);

	    $fct = odbc_result($rs, $fn);

	    $newline = array($fn => $fct);

	    $line = array_merge($line, $newline);

	    //echo $f.": ".$fn."=".$fct."<br>"; 
	}
	unset($line["odbc_affected_rows"]);
	return $line;
    }
    else
    {
	return false;
    }
}

//------------------------------------
//Funcoes voltadas para o KIOSK

function configura_conexoes(){

    if (!isset($_SESSION['sc_caminho'])){
	 $_SESSION['sc_caminho']  = getcwd();
	 $tmp_path                = substr($_SERVER['PHP_SELF'],1);
	 $_SESSION['sc_address']  = '/'.substr($tmp_path,0,strrpos($tmp_path,'/'));
    }

	include $_SESSION['sc_caminho'] ."/../config.php";

	$GLOBALS['Plocation']='';
	$GLOBALS['db']=$database_type;
	if ($GLOBALS['db']=='mssql') {$c_db='m';}
	if ($GLOBALS['db']=='sqlserver') {$c_db='m';}
	if ($GLOBALS['db']=='mysql') {$c_db='y';}
	if ($GLOBALS['db']=='mysqli') {$c_db='y';}
	if ($GLOBALS['db']=='sybase') {$c_db='s';}
	if ($GLOBALS['db']=='oracle') {$c_db='o';}
	
	$_SESSION['con'] = tw_connect($database_server
			   ,$database_username
			   ,$database_password
			   ,$database_name);
   tw_select_db($database_name, $_SESSION['con']);
}

function tw_insert_in_table($strTable = null, $arrayData = Array(), $objConn = null){

	if(is_resource($objConn)===true)
	{
		if(count($arrayData)>=1)
		{
			if(is_null($strTable)!==true)
			{
				$arrayKeys       = array();
				$strSeparador   = '';
				$arrayString     = array(0=>null, 1=> null, 2=> null );
				// pegar os indices
				$arrayKeys       = array_keys($arrayData);
				// cria a string referente aos campos inseridos
				for($i = 0 ;$i < count($arrayKeys); $i++){
					$arrayString[0] = $arrayString[0].$strSeparador.$arrayKeys[$i];
					$arrayString[1] = $arrayString[1].$strSeparador. $arrayData[$arrayKeys[$i]];
					$strSeparador = ', ';
				}
				$arrayString[0] = $strTable.'('.$arrayString[0].')';
				$arrayString[1] = ' VALUES('.$arrayString[1].')';
				$arrayString[2] =  'INSERT INTO '.$arrayString[0].$arrayString[1];
				return tw_query($arrayString[2]);
			}else{
				return false;
			}
		}else{
			return false;
		}
	}else{
		return false;
	}
}

function lpad($campo, $repet, $valor){
	if ($GLOBALS['db']=='oracle'){
		$sql = " LPAD(".$campo.", ".$repet.", '".$valor."') ";
	}elseif($GLOBALS['db']=='mysql'){
		$sql = " LPAD(".$campo.", ".$repet.", '".$valor."') ";
	}elseif($GLOBALS['db']=='mssql' || $GLOBALS['db']=='sqlserver'){
		$sql = " (REPLICATE('".$valor."', (".$repet." - LEN(".$campo."))) + ".$campo.") ";
	}
	return $sql;
}

function tw_mod($campo,$valor){
	if ($GLOBALS['db']=='oracle'){
		$sql = " MOD(".$campo.", ".$valor.") ";
	}elseif($GLOBALS['db']=='mysql'){
		$sql = " MOD(".$campo.", ".$valor.") ";
	}elseif($GLOBALS['db']=='mssql' || $GLOBALS['db']=='sqlserver'){
		$sql = " $campo%$valor" ;
	}
	return $sql;
}
function tw_getTableColumns($table,$con){
	if ($GLOBALS['db']=='oracle'){
		$sql = "
			select COLUMN_NAME      --Nome da Coluna
					, DATA_TYPE        --Tipo de dados. Ex.: Varchar2, Number
					, DATA_LENGTH      --Tamanho do Campo
					, DATA_PRECISION   --Para Campos Number, quantidade de digitos apos a Virgula
					, NULLABLE         --Permite NULL ?? Y/N
				from USER_TAB_COLUMNS
				where upper(table_name) = '".strtoupper($table)."'
				order by COLUMN_ID
		";
		$qry=tw_query($sql,$con);
		$ar_table = array();
		$i=0;
		$sql_constraint = "
			SELECT CONSTRAINT_NAME
				FROM USER_CONSTRAINTS
				WHERE TABLE_NAME='".strtoupper($table)."'
				AND CONSTRAINT_TYPE='P'
		";
		$qry_constraint = tw_query($sql_constraint,$con);
		$row_constraint = tw_fetch_row($qry_constraint);
		$sql_index = "
			SELECT COLUMN_NAME
				FROM USER_IND_COLUMNS
				WHERE INDEX_NAME = '{$row_constraint[0]}'
		";
		$qry_index = tw_query($sql_index,$con);
		$ar_index=array();
		$i=1;
		while ($row_index = tw_fetch_row($qry_index)){
			$ar_index[$i]=$row_index[0];
			$i++;
		}
		while($row=tw_fetch_row($qry)){
			if (array_search($row[0],$ar_index)){
				$pri=1;
			}else{
				$pri=0;
			}
			$ar_table[$row[0]]['DATA_TYPE']		= trim(strtoupper($row[1]));
			$ar_table[$row[0]]['DATA_LENGTH']	= $row[2];
			$ar_table[$row[0]]['DATA_PRECISION']	= intval($row[3]);
			$ar_table[$row[0]]['NULLABLE']		= strtoupper($row[4]);
			$ar_table[$row[0]]['PRIMARY_KEY']	= $pri;
			$ar_table[$row[0]]['AUTO_INCREMENT'] = 0;
		}
		return (count($ar_table)==0)? 0 : $ar_table;
	}else
	if ($GLOBALS['db']=='mysql'){
		$sql = "describe $table";
		if(!$qry = tw_query($sql,$con)){
			return false;
		}
		$ar_table = array();
		$i=0;
		while ($row=tw_fetch_row($qry)){
			$pos = strpos($row[1],'(');
			$type = substr($row[1],0,$pos);
			$resto = substr($row[1],$pos+1);
			if ($pos = strpos($resto,',')){
				$tam = substr($resto,0,$pos);
				$resto = substr($resto,$pos+1);
				$pos2 = strpos($resto,')');
				$dec = substr($resto,0,$pos2);
			}else{
				$pos = strpos($resto,')');
				$tam = substr($resto,0,$pos);
				$dec = 0;
			}
			if ($row[3]=='PRI'){
				$pri=1;
			}else{
				$pri=0;
			}
			$ar_table[$row[0]]['DATA_TYPE']		= trim(strtoupper($type));
			$ar_table[$row[0]]['DATA_LENGTH']	= $tam;
			$ar_table[$row[0]]['DATA_PRECISION']= $dec;
			$ar_table[$row[0]]['NULLABLE']		= strtoupper(substr($row[2],0,1));
			$ar_table[$row[0]]['PRIMARY_KEY']	= $pri;
			if (strtolower($row[5])=='auto_increment'){
				$ar_table[$row[0]]['AUTO_INCREMENT'] = 1;
			}else{
				$ar_table[$row[0]]['AUTO_INCREMENT'] = 0;
			}
			$i++;
		}
		return $ar_table;
	}else
	if ($GLOBALS['db']=='mssql' || $GLOBALS['db']=='sqlserver') {
		$sql = "
			select COLUMN_NAME
					, DATA_TYPE
					, CHARACTER_MAXIMUM_LENGTH
					, NUMERIC_PRECISION
					, NUMERIC_SCALE
					, IS_NULLABLE
				from INFORMATION_SCHEMA.COLUMNS
				where lower(TABLE_NAME) = '".strtolower($table)."'
			order by ORDINAL_POSITION
		";
		//echo $sql;
		$qry = tw_query($sql,$con);
		while($row=tw_fetch_row($qry)){
			if ( (trim(strtoupper($row[1]))=='VARCHAR') ||
					(trim(strtoupper($row[1]))=='CHAR') ){
				$tam = $row[2];
			}else{
				$tam = $row[3];
			}
			$ar_table[strtoupper($row[0])]['DATA_TYPE']			= trim(strtoupper($row[1]));
			$ar_table[strtoupper($row[0])]['DATA_LENGTH']		= $tam;
			$ar_table[strtoupper($row[0])]['DATA_PRECISION']	= intval($row[4]);
			$ar_table[strtoupper($row[0])]['NULLABLE']			= substr(strtoupper($row[5]),0,1);
			$ar_table[strtoupper($row[0])]['PRIMARY_KEY']		= 0;
			$ar_table[strtoupper($row[0])]['AUTO_INCREMENT'] 	= 0;
		}
		return $ar_table;
	}
}

function tw_get_limit_array($sql,$pag,$qtdregs,&$count = 0){
	$inicio = (($pag-1)*$qtdregs)+1;
	$retorno = array();
	if ($GLOBALS['db']=='mysql') {
		//Forma Provisória
		$qry = tw_query($sql,$_SESSION['con']);
		$i=1;
		while($row = tw_fetch_row($qry)){
			if (($i>=$inicio)&&($i<=($inicio+$qtdregs))){
				$retorno[] = $row;
			}else if($i>=($inicio+$qtdregs)){
				break;
			}
			$i++;
		}
	}else if ($GLOBALS['db']=='mssql' || $GLOBALS['db']=='sqlserver'){
		$qtdregs2 = $qtdregs;
		$qtdregs2+= ($inicio-1);
		//-****************************************************
		//  NAO APAGAR ABAIXO
		//
		/*Sql Server 2005 Forma mais Otimizada By Henrique Oliveira */
		$result = preg_replace("/^[Ss][Ee][Ll][Ee][Cc][Tt]/","SELECT * FROM ( SELECT row_number() OVER (changeby) AS rn, ",$sql);
		$result = preg_replace("/[Gg][Rr][Oo][Uu][Pp][\ ][Bb][Yy]/","group by ",$result);
		$result = preg_replace("/\n/","",$result);
		$result = preg_replace("/(changeby)(.*)([Oo][Rr][Dd][Ee][Rr].*$)/","$3 $2",$result);
		$result.=") AS novosql WHERE novosql.rn BETWEEN $inicio AND $qtdregs2";
		/*///-********************************************************************/
		
		//Forma Provis󲩡
		//echo "<pre>$result";
		if(!$qry = tw_query($result,$_SESSION['con'])){
			$qry = tw_query($sql,$_SESSION['con']);
			$i=1;
			while($row = tw_fetch_row($qry)){
				if (($i>=$inicio)&&($i<($inicio+$qtdregs))){

					$retorno[] = $row;
				}else if($i>=($inicio+$qtdregs)){
					break;
				}
				$i++;
			}
		}else{
			while($row = tw_fetch_row($qry)){
				unset($row[0]);
				$retorno[] = array_values($row);
			}
		}
	}else if ($GLOBALS['db']=='oracle'){
		$qtdregs+= ($inicio-1);
		//Oracle Forma Otimizada By Henrique Oliveira
		$result = preg_replace("/^[Ss][Ee][Ll][Ee][Cc][Tt]/","SELECT * FROM (SELECT a.*, rownum RN FROM (SELECT ",$sql);
		$result.= ") a WHERE rownum <=$qtdregs) WHERE rn >=$inicio";
		//echo "<pre>$result";
		if(!$qry = tw_query($result,$_SESSION['con'])){

		}
		while($row = tw_fetch_row($qry)){
			//unset($row[0]);
			$retorno[] = array_values($row);
		}
	}
	//Retornar um Count do Select
	$sql_count = preg_replace("/\n/","",$sql);
	$sql_count = preg_replace("/[Ff][Rr][Oo][Mm]/i","From\n",$sql_count);
	$sql_count = preg_replace("/^[Ss][Ee][Ll][Ee][Cc][Tt].*[Ff][Rr][Oo][Mm]/i","Select Count(*) from ",$sql_count);
	$sql_count = preg_replace("/[Gg][Rr][Oo][Uu][Pp][\ ][Bb][Yy].*$/i","",$sql_count);
	//echo $sql_count;exit;
	$qry = tw_query($sql_count,$_SESSION['con']);
	$row = tw_fetch_row($qry);
	$count = $row[0];
	return $retorno;
}


function tw_fields_info($res){
	$i = 0;
	if ($GLOBALS['db']=='sqlserver'){
		foreach( sqlsrv_field_metadata( $res ) as $InfoCampo ) {
			$nomes[$i] = $InfoCampo;
			$i++;
		}

	}else if ($GLOBALS['db']=='mssql'){


	}
	return $nomes;

}
